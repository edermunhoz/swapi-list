import api from './../api';

export default async function getCharacters(page = 1) {
  try {
    const response = await api.get(`/people/?page=${page}`);
    return response.data;
  } catch (error) {
    console.log(error);
    window.location.href = '/not-found';
  }

}