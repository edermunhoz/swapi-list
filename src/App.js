import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import Header from './components/layout/header';
import List from './pages/list';
import NoMatch from './pages/not-found';

class App extends Component {
  render() {
    return (
      <>
        <Header />
        <Switch>
          <Route exact path="/" component={List} />
          <Route component={NoMatch} />
        </Switch>
      </>
    );
  }
}

export default App;
