import React, { Component } from 'react';
import notFound from './../../assets/img/darth-vader-header.jpg';
import { Link } from 'react-router-dom';
import './style.scss';

class NoMatch extends Component {

	render() {
		return (
			<div className="page-not-found">
				<img src={notFound} alt='not found' className="img-not-found" />
				<h1 className="title">404</h1>
				<p className="text-lead">Você substimou o poder do lado negro da força.</p>
				<Link to='/' className="btn btn-dark">Voltar para página inicial</Link>
			</div>
		)
	};

};

export default NoMatch;
