import React, { Component } from 'react';
import getCharacters from './../../services/characters';
import Paginate from './../../components/shareds/paginate';
import Spinner from './../../components/shareds/spinner';
import './style.scss';

class List extends Component {
  constructor(props) {
    super(props);
    this.updatePage = this.updatePage.bind(this);
    this.state = {
      characters: [],
      prevPage: '',
      nextPage: ''
    };
  }

  componentDidMount() {
    this.loadCharacteres();
  }

  loadCharacteres(page) {
    getCharacters(page)
      .then(response => {
        this.setState({
          characters: response.results,
          prevPage: response.previous,
          nextPage: response.next
        });
      })
      .catch(error => {
        console.log(error);
      })
  }

  updatePage(pagNumber) {
    this.setState({ characters: '' });
    this.loadCharacteres(pagNumber);
  }

  render() {
    const { characters, nextPage, prevPage } = this.state;

    if (!characters.length) return <Spinner />;

    return (
      <>
        <ol className="character-list">
          {characters.map((character, i) => (
            <li key={i}>{character.name}</li>
          ))
          }
        </ol>
        <Paginate next={nextPage} prev={prevPage} setPage={(pagNumber) => this.updatePage(pagNumber)} />
      </>
    );
  }
}

export default List;
