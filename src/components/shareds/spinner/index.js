import React from 'react';
import './style.scss';

const Spinner = () => {
  return (
    <div className="wrapper-spinner">
      <div className="spinner"></div>
    </div>
  );
}

export default Spinner;
