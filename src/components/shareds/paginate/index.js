import React, { Component } from 'react';
import './../../../assets/stylesheets/buttons/style.scss';
import './style.scss';

class Paginate extends Component {
  constructor(props) {
    super(props);
    this.nextPage = this.nextPage.bind(this);
    this.prevPage = this.prevPage.bind(this);
  }

  prevPage() {
    let prevPage = this.getParameterByName('page', this.props.prev);
    this.props.setPage(prevPage);
  }

  nextPage() {
    let nextPage = this.getParameterByName('page', this.props.next);
    this.props.setPage(nextPage);
  }

  getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[[\]]/g, '\\$&');
    const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

  render() {
    return (
      <div className="paginate">
        <button onClick={this.prevPage} className="btn btn-primary" disabled={this.props.prev === null}>Anterior</button>
        <button onClick={this.nextPage} className="btn btn-primary" disabled={this.props.next === null}>Próxima</button>
      </div>
    );
  }
}

export default Paginate;
