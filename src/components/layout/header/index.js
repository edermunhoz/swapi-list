import React, { Component } from 'react';
import './style.scss';

class Header extends Component {
  render() {
    return (
      <header className="main-header">
        <h1 className="title">Star Wars</h1>
      </header>
    );
  }
}

export default Header;
